// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'ngCordova', 'ionic.service.core', 'ionic.service.push'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }

     Ionic.io();



    // var push = new Ionic.Push({});

    // push.register(function(token) {
    //   // Log out your device token (Save this!)
    //   console.log("Got Token:",token.token);
    // });

  });
})
.controller("AppCtrl", function($scope, $ionicPlatform, $ionicPush){

  $scope.log = "Starting";

  $scope.registerMe = function(){

    $ionicPlatform.ready(function(){

try{

  var user = Ionic.User.current();
$scope.log +="ionic user: "+ JSON.stringify(user);  
if (!user.id) {
          // user.id = Ionic.User.anonymousId();
          user.id = 'user-1';
          user.save();
          $scope.log += "user id saved";
        }

  $ionicPush.init({
  "debug": true,
  "onNotification": function(notification) {
    $scope.log +=" notification: " + JSON.stringify(notification);
    var payload = notification.payload;
    console.log(notification, payload);
  },
  "onRegister": function(data) {
    $scope.log +="registered token: " +data.token;
    user.addPushToken(data.token);
    user.save();
    
  }
});

$ionicPush.register();


      // var push = new Ionic.Push();
      // var user = Ionic.User.current();

      // if (!user.id) {
      //     // user.id = Ionic.User.anonymousId();
      //     user.id = 'user-1';
      //   }
      //   user.save();

      // var callback = function(pushToken) {
      //   console.log('Registered token:', pushToken.token);
      //   user.addPushToken(pushToken);
      //   user.save(); // you NEED to call a save after you add the token
      // }

      // push.register(callback);

      // console.log('registered for push!');

    }catch(err){

$scope.log += "Error: " + JSON.stringify(err);

    }

  
       // Ionic.io();
       // console.log('io initialized');

       //  // this will give you a fresh user or the previously saved 'current user'
       //  var user = Ionic.User.current();

       // console.log('ionic user: ' + JSON.stringify(user));
       //  // if the user doesn't have an id, you'll need to give it one.
       //  if (!user.id) {
       //    // user.id = Ionic.User.anonymousId();
       //    user.id = 'user-1';
       //  }

       //  //persist the user
       //  user.save().then(function(){console.log('saved ok')}, function(err){console.log("Ionic user sasve err: " + err));})


    });


  }

})
